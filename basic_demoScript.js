import * as fs from 'fs';
import * as ipfsClient from "ipfs-http-client";
import readline from 'readline-promise';
import conf from './conf.js';
import crypto from 'crypto';
import { JsonDB, Config } from 'node-json-db';

const rlp = readline.default.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: true
});

import { InMemorySigner } from "@taquito/signer";
import { TezosToolkit } from "@taquito/taquito";
import { char2Bytes } from "@taquito/utils";

import { deployAndMintNFT, declareAndConfirmTransporterForNFT, createDVPAndProceedToExchange, transporterDeliveryDone } from "./trailComponents.js";

import * as permitCode from "./contracts/permits.js";
import * as tkEurCode from "./contracts/tkEur.js";
import * as materialNFT from "./contracts/product.js";
import * as DVPCode from "./contracts/DVP.js";

const config = JSON.parse(fs.readFileSync('./.taq/config.json'));

const db = new JsonDB(new Config("contractDB", true, true, '/'));

function _checkIfContractDeployed(originTx, originContract) {
    if (!originTx.hash || !originContract) {
        throw new Error('Contract not deployed !');
    }
}

async function _updateJson() {
    db.push(`/${jsonContent.uuid}/json-content`, jsonContent);
    db.push(`/${opJsonContent.uuid}/op`, opJsonContent);
}

let Tezos;
let signer;
let farmerSigner;
let roadTransportSigner;
let factorySigner;
let storeSigner;
let bidonMakerSigner;

// Accounts Sandbox
// const { alice, bob, john, jane, joe, mike, dave } = config.sandbox.local.accounts;
// const { rpcUrl } = config.sandbox.local;

// Accounts Ghostnet
const { alice, bob, john, jane, joe, mike, dave } = config[conf.CHAIN_TYPE].local.accounts;
const { rpcUrl } = config[conf.CHAIN_TYPE].local;

// User mapping
const farmer = bob;
const bidonMaker = mike;
const roadTransport = john;
const factory = jane;
const store = dave;

// contracts
let permits;
let token;
let palmCropNFT;
let soapNFT;
let brassContainerNFT;


const contractUUID = crypto.randomUUID();

let jsonContent = {
    uuid: contractUUID,
    materialNFT: {}, token: "", wallet: {
        farmer: farmer.publicKeyHash,
        roadTransport: roadTransport.publicKeyHash,
        factory: factory.publicKeyHash,
        store: store.publicKeyHash,
    }
};

let opJsonContent = { uuid: contractUUID }

const uploadData = async (data) => {
    const projectId = conf.IPFS_PROJECT_ID;
    const projectSecret = conf.IPFS_PROJECT_SECRET;
    const auth =
        "Basic " +
        Buffer.from(projectId + ":" + projectSecret).toString("base64");

    const client = ipfsClient.create({
        host: "ipfs.infura.io",
        port: 5001,
        protocol: "https",
        headers: {
            authorization: auth,
        },
    });
    const { path } = await client.add(JSON.stringify(data));
    return `ipfs://${path}`;
}

async function initSandboxPermit() {
    Tezos = new TezosToolkit(rpcUrl);
    signer = new InMemorySigner(alice.secretKey.replace(/unencrypted:/, ""));
    farmerSigner = new InMemorySigner(farmer.secretKey.replace(/unencrypted:/, ""));
    roadTransportSigner = new InMemorySigner(roadTransport.secretKey.replace(/unencrypted:/, ""));
    factorySigner = new InMemorySigner(factory.secretKey.replace(/unencrypted:/, ""));
    storeSigner = new InMemorySigner(store.secretKey.replace(/unencrypted:/, ""));
    bidonMakerSigner = new InMemorySigner(bidonMaker.secretKey.replace(/unencrypted:/, ""));
    Tezos.setSignerProvider(signer);
    const permitOp = await Tezos.contract.originate({
        code: permitCode.code,
        init: permitCode.getStorage(alice.publicKeyHash),
    });
    const permitContract = await permitOp.contract();
    _checkIfContractDeployed(permitOp, permitContract);
    permits = permitContract;
    console.log('Permits deployed', permits.address);

    const tokenOp = await Tezos.contract.originate({
        code: tkEurCode.code,
        init: tkEurCode.getStorage(
            alice.publicKeyHash,
            permits.address,
        ),
    });
    const tokenContract = await tokenOp.contract();
    _checkIfContractDeployed(tokenOp, tokenContract);
    token = tokenContract;
    console.log('EurTk deployed', token.address);
    let tx = await permits.methods.add(token.address).send();
    await tx.confirmation(1);
    console.log('EurTk added to permit');
    tx = await token.methods.mint(roadTransport.publicKeyHash, 20000).send();
    await tx.confirmation(1);
    tx = await token.methods.mint(factory.publicKeyHash, 50000).send();
    await tx.confirmation(1);
    tx = await token.methods.mint(store.publicKeyHash, 50000).send();
    await tx.confirmation(1);
    console.log('Roadtransport and factory EurTk mint');
    jsonContent.token = token.address;
    await _updateJson();
}

// 1
async function deployNFTAndMint(ownerSigner, materialNFTCode, nftOwner, permitsNFT, nftSigner, contractName, contractType, name, owner) {
    const { contract, mintHash } = await deployAndMintNFT(Tezos, ownerSigner, materialNFTCode, nftOwner, permitsNFT, nftSigner, contractName, contractType, name, owner, {
        "made": Date.now() / 1000,
        "position": "5°24’11.3 »N 1°27’29.6 »W",
        "label": [
            "Responsible"
        ]
    });
    opJsonContent[contract.address] = {};
    opJsonContent[contract.address].mint = mintHash;
    await _updateJson();
    return contract;
}

async function transfertTemporaryHolderToRoadTransporter(nftContract, nftId, ownerSigner, transporterAddress, transporterSigner, name, owner) {
    const transporterTx = await declareAndConfirmTransporterForNFT(Tezos, nftContract, nftId, ownerSigner, transporterAddress, transporterSigner, name, owner);
    opJsonContent[nftContract.address].setTransporter = transporterTx;
    await _updateJson();
}

async function createDVP(DVPName, DVPSigner, DVPCode, legA, legB) {
    const DVPdata = await createDVPAndProceedToExchange(Tezos, DVPSigner, DVPCode, legA, legB);
    jsonContent.materialNFT[DVPName] = DVPdata.DVPAddress;
    opJsonContent[legA.contract.address].DVP = { creation: DVPdata.creationHash, from: legA.walletDebtor.publicKeyHash, to: legA.walletCreditor.publicKeyHash };
    opJsonContent[legA.contract.address].DVP.confirmed = DVPdata.confirmedHash;
    await _updateJson();
}

async function transporterDeliveryCompleted(transporterSigner, nftContract, nftId) {
    const transporterTx = await transporterDeliveryDone(Tezos, transporterSigner, nftContract, nftId)
    opJsonContent[palmCropNFT.address].transporterDone = transporterTx;
    await _updateJson();
}

async function factoryBrassContainerPalmOil() {
    const { contract, mintHash } = await deployAndMintNFT(Tezos, signer, materialNFT, factory.publicKeyHash, permits, factorySigner, "Brass Container with Palm oil", "factory", "GH Factory", "John Doe", {
        "transaction": "oopGHtGMMpHB9FAp1wUPgkrLTpphCMiYeVWufhxHPxzR5z3eQZY",
        "position": "5°24’11.3 »N 1°27’29.6 »W",
        "label": ["Responsible Production"]
    });
    soapNFT = contract;
    opJsonContent[contract.address] = {};
    opJsonContent[contract.address].mint = mintHash;
    jsonContent.materialNFT.soapNFT = soapNFT.address;
    const url = await uploadData({
        used: "100%",
    });
    const metadataByte = char2Bytes(url);
    let tx = await soapNFT.methods.update_composed(1, [{ contract_nft: palmCropNFT.address, id_nft: 1, material_metadata: metadataByte }, { contract_nft: brassContainerNFT.address, id_nft: 1, material_metadata: metadataByte }]).send();
    await tx.confirmation(1);
    opJsonContent[soapNFT.address].composition = tx.hash;
    await _updateJson();
    tx = await soapNFT.methods.update_token_isReady(1, true).send();
    await tx.confirmation(1);
}


async function main() {
    await initSandboxPermit();
    palmCropNFT = await deployNFTAndMint(signer, materialNFT, farmer.publicKeyHash, permits, farmerSigner, "Palm oil by Kilimo", "farm", "Oka", "Kilimo");
    await transfertTemporaryHolderToRoadTransporter(palmCropNFT, 1, farmerSigner, roadTransport.publicKeyHash, roadTransportSigner, "Sal", "Cal"); // Palm Crop 1 NFT
    await createDVP("DVPFarmerFactory", farmerSigner, DVPCode,
        { walletDebtor: farmer, signer: farmerSigner, walletCreditor: factory, contract: palmCropNFT, id: 1, amount: 1 },
        { walletDebtor: factory, signer: factorySigner, walletCreditor: farmer, contract: token, id: 0, amount: 250 });
    // Part 2
    console.log('Deploy bidonMaker');
    brassContainerNFT = await deployNFTAndMint(signer, materialNFT, bidonMaker.publicKeyHash, permits, bidonMakerSigner, "Brass container by Gol", "brassContainer", "Pol", "Gol");
    await transporterDeliveryCompleted(roadTransportSigner, palmCropNFT, 1);
    console.log('Transport completed');
    await factoryBrassContainerPalmOil();
    await createDVP("DVPFactoryStore", factorySigner, DVPCode,
        { walletDebtor: factory, signer: factorySigner, walletCreditor: store, contract: soapNFT, id: 1, amount: 1 },
        { walletDebtor: store, signer: storeSigner, walletCreditor: factory, contract: token, id: 0, amount: 3000 });
    console.log('Ending');
    process.exit();
}

main();
