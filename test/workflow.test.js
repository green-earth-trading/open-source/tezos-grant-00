import { InMemorySigner } from "@taquito/signer";
import { TezosToolkit } from "@taquito/taquito";
import config from "../.taq/config.json";
import { jest } from "@jest/globals";
import { char2Bytes } from "@taquito/utils";

import * as permitCode from "../contracts/permits.js";
import * as tkEurCode from "../contracts/tkEur.js";
import * as productNFT from "../contracts/product.js";
import * as DVPCode from "../contracts/DVP.js";


function _checkIfContractDeployed(originTx, originContract) {
  expect(originTx.hash).toBeDefined();
  expect(originTx.includedInBlock).toBeLessThan(Number.POSITIVE_INFINITY);
  expect(originContract).toBeDefined();
}

let Tezos;
let signer;
let farmerSigner;
let roadTransportSigner;
let factorySigner;

const metadataByte = char2Bytes("https://ipfs.io/ipfs/QmQg4tGaYkGPJGH8MvFuMFSDce5du3rR6xSAs1bWK6itz9");

// Accounts
const { alice, bob, john, jane, joe, mike, dave } = config.sandbox.local.accounts;
const { rpcUrl } = config.sandbox.local;
jest.setTimeout(80000);

// User mapping
const farmer = bob;
const roadTransport = john;
const factory = jane;
const navalTransport = joe;
const distribCenter = mike;
const store = dave;

// contracts
let whitelistStorage;
let whitelist;
let permits;
let token;
let palmCropNFT;
let palmOilNFT;
let cardboardNFT;
let DVPFarmerFactory;
let DVPRoadTransportFactory;


describe("Originate base contract", () => {
  beforeAll(() => {
    Tezos = new TezosToolkit(rpcUrl);
    signer = new InMemorySigner(alice.secretKey.replace(/unencrypted:/, ""));
    farmerSigner = new InMemorySigner(farmer.secretKey.replace(/unencrypted:/, ""));
    roadTransportSigner = new InMemorySigner(roadTransport.secretKey.replace(/unencrypted:/, ""));
    factorySigner = new InMemorySigner(factory.secretKey.replace(/unencrypted:/, ""));
    Tezos.setSignerProvider(signer);
  });

  test("Deploy permits", async () => {
    const permitOp = await Tezos.contract.originate({
      code: permitCode.code,
      init: permitCode.getStorage(alice.publicKeyHash),
    });
    const permitContract = await permitOp.contract();
    _checkIfContractDeployed(permitOp, permitContract);
    permits = permitContract;
  });
  test("Deploy eurTk", async () => {
    const tokenOp = await Tezos.contract.originate({
      code: tkEurCode.code,
      init: tkEurCode.getStorage(
        alice.publicKeyHash,
        permits.address,
      ),
    });
    const tokenContract = await tokenOp.contract();
    _checkIfContractDeployed(tokenOp, tokenContract);
    token = tokenContract;
  });
  test("Add token as consumer", async () => {
    const tx = await permits.methods.add(token.address).send();
    await tx.confirmation(1);
  });
});

describe("Mint token for users", () => {
  it("Mint token for roadTransport", async () => {
    const tx = await token.methods.mint(roadTransport.publicKeyHash, 20000).send();
    await tx.confirmation(1);
  });
  it("Mint token for factory", async () => {
    const tx = await token.methods.mint(factory.publicKeyHash, 50000).send();
    await tx.confirmation(1);
  });
});

describe("Minting and trading of NFT", () => {
  it("Farmer should deploy his NFT for PalmCrop", async () => {
    const palmCropOp = await Tezos.contract.originate({
      code: productNFT.code,
      init: productNFT.getStorage(
        farmer.publicKeyHash,
        permits.address,
      ),
    });
    const contract = await palmCropOp.contract();
    _checkIfContractDeployed(palmCropOp, contract);
    palmCropNFT = contract;
    const tx = await permits.methods.add(palmCropNFT.address).send();
    await tx.confirmation(1);
  });
  it("Farmer should mint a NFT representing a crop", async () => {
    // Ipfs link contain data about the crop  (Harvest date, position, ect...);
    await Tezos.setSignerProvider(farmerSigner);
    const tx = await palmCropNFT.methods.mint(farmer.publicKeyHash, { "": metadataByte }, true).send();
    await tx.confirmation(1);
    await Tezos.setSignerProvider(signer);
  });
  it("Farmer should own the NFT representing a crop", async () => {
    // Ipfs link contain data about the crop  (Harvest date, position, ect...);
    let balance = await palmCropNFT.views.balance_of([{ owner: farmer.publicKeyHash, token_id: 1 }]).read();
    balance = +balance[0].balance;
    expect(balance).toEqual(1);
  });
});

describe("Demo Flow", () => {
  it("Farmer should assign the transporter as the temporary holder", async () => {
    await Tezos.setSignerProvider(farmerSigner);
    const tx = await palmCropNFT.methods.declare_transporter(1, roadTransport.publicKeyHash).send();
    await tx.confirmation(1);
    const storage = await palmCropNFT.storage();
    const candidateHolder = await storage.temporary_transporter.get([1]);
    expect(candidateHolder.candidate_transporter).toEqual({ candidate_address: roadTransport.publicKeyHash, transporter_metadata: "" });
  });
  it("Road transporter confirm the temporary hold", async () => {
    await Tezos.setSignerProvider(roadTransportSigner);
    const tx = await palmCropNFT.methods.confirm_transporter(1, metadataByte).send();
    await tx.confirmation(1);
    const storage = await palmCropNFT.storage();
    const candidateHolder = await storage.temporary_transporter.get([1]);
    expect(candidateHolder.candidate_transporter).toEqual(null);
    expect(candidateHolder.current_transporter).toEqual({ "candidate_address": roadTransport.publicKeyHash, "transporter_metadata": metadataByte });
  });
  it("Road transporter should confirm the delivery", async () => {
    await Tezos.setSignerProvider(roadTransportSigner);
    const tx = await palmCropNFT.methods.delivery_done(1).send();
    await tx.confirmation(1);
    const storage = await palmCropNFT.storage();
    const candidateHolder = await storage.temporary_transporter.get([1]);
    expect(candidateHolder.candidate_transporter).toEqual(null);
    expect(candidateHolder.current_transporter).toEqual(null);
    expect(candidateHolder.previous_transporter).toEqual([{ "candidate_address": roadTransport.publicKeyHash, "transporter_metadata": metadataByte }]);
  });
  it("Create DVP between Farmer and Factory", async () => {
    const DVPOp = await Tezos.contract.originate({
      code: DVPCode.code,
      init: DVPCode.getStorage(
        palmCropNFT.address,
        1,
        1,
        farmer.publicKeyHash,
        factory.publicKeyHash,
        token.address,
        0,
        250,
        factory.publicKeyHash,
        farmer.publicKeyHash
      ),
    });
    const DVPContract = await DVPOp.contract();
    _checkIfContractDeployed(DVPOp, DVPContract);
    DVPFarmerFactory = DVPContract;
  });
  it("farmer grant DVP authorisation to move CROP", async () => {
    const storage = await palmCropNFT.storage();
    const initialOperators = await storage.operator.get([
      DVPFarmerFactory.address,
      1,
      farmer.publicKeyHash,
    ]);
    expect(initialOperators).toBeUndefined();
    // Farmer to make the transaction
    await Tezos.setSignerProvider(farmerSigner);

    const tx = await palmCropNFT.methods
      .update_operators([
        {
          add_operator: {
            owner: farmer.publicKeyHash,
            operator: DVPFarmerFactory.address,
            token_id: 1,
          },
        },
      ])
      .send();
    await tx.confirmation(1);
    const storageAfter = await palmCropNFT.storage();
    const afterOperators = await storageAfter.operator.get([
      DVPFarmerFactory.address,
      1,
      farmer.publicKeyHash,
    ]);
    expect(afterOperators).toBeDefined();
  });
  it("Factory grant DVP authorisation to move funds", async () => {
    const storage = await token.storage();
    const initialOperators = await storage.operator.get([
      DVPFarmerFactory.address,
      0,
      factory.publicKeyHash,
    ]);
    expect(initialOperators).toBeUndefined();
    await Tezos.setSignerProvider(factorySigner);

    const tx = await token.methods
      .update_operators([
        {
          add_operator: {
            owner: factory.publicKeyHash,
            operator: DVPFarmerFactory.address,
            token_id: 0,
          },
        },
      ])
      .send();
    await tx.confirmation(1);
    const storageAfter = await token.storage();
    const afterOperators = await storageAfter.operator.get([
      DVPFarmerFactory.address,
      0,
      factory.publicKeyHash,
    ]);
    expect(afterOperators).toBeDefined();
  });
  test("Proceed to exchange should work", async () => {
    await Tezos.setSignerProvider(signer);

    let balanceBeforeA = await palmCropNFT.views
      .balance_of([{ owner: farmer.publicKeyHash, token_id: 1 }])
      .read();
    balanceBeforeA = +balanceBeforeA[0].balance;
    let balanceBeforeB = await token.views
      .balance_of([{ owner: factory.publicKeyHash, token_id: 0 }])
      .read();
    balanceBeforeB = +balanceBeforeB[0].balance;
    const tx = await DVPFarmerFactory.methods.default().send();
    await tx.confirmation(1);

    let balanceAfterA = await palmCropNFT.views
      .balance_of([{ owner: farmer.publicKeyHash, token_id: 1 }])
      .read();
    balanceAfterA = +balanceAfterA[0].balance;
    let balanceAfterB = await token.views
      .balance_of([{ owner: factory.publicKeyHash, token_id: 0 }])
      .read();
    balanceAfterB = +balanceAfterB[0].balance;

    expect(balanceAfterA).toEqual(0);
    expect(balanceAfterB).toEqual(balanceBeforeB - 250);

    // Road transport should own the NFT
    let balanceRT = await palmCropNFT.views.balance_of([{ owner: factory.publicKeyHash, token_id: 1 }]).read();
    balanceRT = +balanceRT[0].balance;
    expect(balanceRT).toEqual(1);

    let balanceFarmer = await token.views
      .balance_of([{ owner: farmer.publicKeyHash, token_id: 0 }])
      .read();
    balanceFarmer = +balanceFarmer[0].balance;
    expect(balanceFarmer).toEqual(250);

  });
  test("Factory sould create a new NFT Contract with PalmCropNFT", async () => {
    await Tezos.setSignerProvider(factorySigner);
    const palmCropOp = await Tezos.contract.originate({
      code: productNFT.code,
      init: productNFT.getStorage(
        factory.publicKeyHash,
        permits.address,
      ),
    });
    const contract = await palmCropOp.contract();
    _checkIfContractDeployed(palmCropOp, contract);
    palmOilNFT = contract;
    // const tx = await permits.methods.add(palmOilNFT.address).send();
    // await tx.confirmation(1);
  });
  test("For the sake of example we will mint a CARDBOARD NFT that will be used to create palm oil", async () => {
    await Tezos.setSignerProvider(factorySigner);
    const palmCropOp = await Tezos.contract.originate({
      code: productNFT.code,
      init: productNFT.getStorage(
        factory.publicKeyHash,
        permits.address,
      ),
    });
    const contract = await palmCropOp.contract();
    _checkIfContractDeployed(palmCropOp, contract);
    cardboardNFT = contract;
    // const tx = await permits.methods.add(cardboardNFT.address).send();
    // await tx.confirmation(1);
  });
  test("Factory mint a CardBoard NFT", async () => {
    const tx = await cardboardNFT.methods.mint(factory.publicKeyHash, { "": metadataByte }, true).send();
    await tx.confirmation(1);
  });
  test("Factory mint a Palmoil NFT", async () => {
    const tx = await palmOilNFT.methods.mint(factory.publicKeyHash, { "": metadataByte }, false).send();
    await tx.confirmation(1);
  });
  test("Factory used his PalmOil and Cardboard NFT for the PalmOil NFT", async () => {
    const tx = await palmOilNFT.methods.update_composed(1, [{ contract_nft: palmCropNFT.address, id_nft: 1, material_metadata: metadataByte }, { contract_nft: cardboardNFT.address, id_nft: 1, material_metadata: metadataByte }]).send();
    await tx.confirmation(1);
  });
  test("Should not be able to transfer an NFT when not ready", async () => {
    let error;
    try {
      const tx = await palmOilNFT.methods
        .transfer([
          {
            from_: factory.publicKeyHash,
            txs: [{ to_: store.publicKeyHash, token_id: 1, amount: 1 }],
          },
        ]).send();
      await tx.confirmation(1);
    } catch (e) {
      error = e;
    }
    expect(error.message).toEqual("IS_NOT_READY");
  });

  test("Set PalmOil NFT to ready", async () => {
    const tx = await palmOilNFT.methods.update_token_isReady(1, true).send();
    await tx.confirmation(1);
  });

  test("Retrieve PalmOil composition", async () => {
    let composition = await palmOilNFT.views.get_components(1).read();
    expect(composition.length).toEqual(2);
    expect(composition[0].contract_nft).toEqual(palmCropNFT.address);
    expect(composition[1].contract_nft).toEqual(cardboardNFT.address);
  });

  test("Factory should send the Palmoil to the store", async () => {
    const tx = await palmOilNFT.methods
      .transfer([
        {
          from_: factory.publicKeyHash,
          txs: [{ to_: store.publicKeyHash, token_id: 1, amount: 1 }],
        },
      ]).send();
    await tx.confirmation(1);
  })
})
