import { InMemorySigner } from "@taquito/signer";
import { TezosToolkit } from "@taquito/taquito";
import config from "../.taq/config.json";
import { jest } from "@jest/globals";
import { char2Bytes } from "@taquito/utils";

import * as permitCode from "../contracts/permits.js";
import * as tkEurCode from "../contracts/tkEur.js";
import * as productNFT from "../contracts/product.js";
import * as DVPCode from "../contracts/DVP.js";


function _checkIfContractDeployed(originTx, originContract) {
  expect(originTx.hash).toBeDefined();
  expect(originTx.includedInBlock).toBeLessThan(Number.POSITIVE_INFINITY);
  expect(originContract).toBeDefined();
}

let Tezos;
let signer;
let farmerSigner;
let roadTransportSigner;
let factorySigner;

const metadataByte = char2Bytes("https://ipfs.io/ipfs/QmQg4tGaYkGPJGH8MvFuMFSDce5du3rR6xSAs1bWK6itz9");

// Accounts
const { alice, bob, john, jane, joe, mike, dave } = config.sandbox.local.accounts;
const { rpcUrl } = config.sandbox.local;
jest.setTimeout(80000);

// User mapping
const farmer = bob;
const roadTransport = john;
const factory = jane;
const navalTransport = joe;
const distribCenter = mike;
const store = dave;

// contracts
let whitelistStorage;
let whitelist;
let permits;
let token;
let palmCropNFT;
let palmOilNFT;
let cardboardNFT;
let DVPFarmerRoadTransport;
let DVPRoadTransportFactory;


describe("Originate base contract", () => {
  beforeAll(() => {
    Tezos = new TezosToolkit(rpcUrl);
    signer = new InMemorySigner(alice.secretKey.replace(/unencrypted:/, ""));
    farmerSigner = new InMemorySigner(farmer.secretKey.replace(/unencrypted:/, ""));
    roadTransportSigner = new InMemorySigner(roadTransport.secretKey.replace(/unencrypted:/, ""));
    factorySigner = new InMemorySigner(factory.secretKey.replace(/unencrypted:/, ""));
    Tezos.setSignerProvider(signer);
  });

  test("Deploy permits", async () => {
    const permitOp = await Tezos.contract.originate({
      code: permitCode.code,
      init: permitCode.getStorage(alice.publicKeyHash),
    });
    const permitContract = await permitOp.contract();
    _checkIfContractDeployed(permitOp, permitContract);
    permits = permitContract;
  });
  test("Deploy eurTk", async () => {
    const tokenOp = await Tezos.contract.originate({
      code: tkEurCode.code,
      init: tkEurCode.getStorage(
        alice.publicKeyHash,
        permits.address,
      ),
    });
    const tokenContract = await tokenOp.contract();
    _checkIfContractDeployed(tokenOp, tokenContract);
    token = tokenContract;
  });
  test("Add token as consumer", async () => {
    const tx = await permits.methods.add(token.address).send();
    await tx.confirmation(1);
  });
});

describe("Mint token for users", () => {
  it("Mint token for roadTransport", async () => {
    const tx = await token.methods.mint(roadTransport.publicKeyHash, 20000).send();
    await tx.confirmation(1);
  });
  it("Mint token for factory", async () => {
    const tx = await token.methods.mint(factory.publicKeyHash, 50000).send();
    await tx.confirmation(1);
  });
});

describe("Minting and trading of NFT", () => {
  it("Farmer should deploy his NFT for PalmCrop", async () => {
    const palmCropOp = await Tezos.contract.originate({
      code: productNFT.code,
      init: productNFT.getStorage(
        farmer.publicKeyHash,
        permits.address,
      ),
    });
    const contract = await palmCropOp.contract();
    _checkIfContractDeployed(palmCropOp, contract);
    palmCropNFT = contract;
    const tx = await permits.methods.add(palmCropNFT.address).send();
    await tx.confirmation(1);
  });
  it("Farmer should mint a NFT representing a crop", async () => {
    // Ipfs link contain data about the crop  (Harvest date, position, ect...);
    await Tezos.setSignerProvider(farmerSigner);
    const tx = await palmCropNFT.methods.mint(farmer.publicKeyHash, { "": metadataByte }, true).send();
    await tx.confirmation(1);
    await Tezos.setSignerProvider(signer);
  });
  it("Farmer should own the NFT representing a crop", async () => {
    // Ipfs link contain data about the crop  (Harvest date, position, ect...);
    let balance = await palmCropNFT.views.balance_of([{ owner: farmer.publicKeyHash, token_id: 1 }]).read();
    balance = +balance[0].balance;
    expect(balance).toEqual(1);
  });
});

describe("Temporary holder test", () => {
  it("Incorrect token holder should not be able to set a temporary holder", async () => {
    await Tezos.setSignerProvider(roadTransportSigner);
    let err;
    try {
      const tx = await palmCropNFT.methods.declare_transporter(1, roadTransport.publicKeyHash).send();
      await tx.confirmation(1);
    } catch (e) {
      err = e;
    }
    expect(err.message).toEqual("CALLER_NOT_OWNER");
  });
  it("Farmer should assign the transporter as the temporary holder", async () => {
    await Tezos.setSignerProvider(farmerSigner);
    const tx = await palmCropNFT.methods.declare_transporter(1, roadTransport.publicKeyHash).send();
    await tx.confirmation(1);
    const storage = await palmCropNFT.storage();
    const candidateHolder = await storage.temporary_transporter.get([1]);
    expect(candidateHolder.candidate_transporter).toEqual({ candidate_address: roadTransport.publicKeyHash, transporter_metadata: "" });

  });
  it("Non candidate token holder should not be able to accept", async () => {
    let err;
    await Tezos.setSignerProvider(factorySigner);
    try {
      const tx = await palmCropNFT.methods.confirm_transporter(1, metadataByte).send();
      await tx.confirmation(1);
    } catch (e) {
      err = e;
    }
    expect(err.message).toEqual("INVALID CANDIDATE");
  })
  it("Road transporter confirm the temporary hold", async () => {
    await Tezos.setSignerProvider(roadTransportSigner);
    const tx = await palmCropNFT.methods.confirm_transporter(1, metadataByte).send();
    await tx.confirmation(1);
    const storage = await palmCropNFT.storage();
    const candidateHolder = await storage.temporary_transporter.get([1]);
    expect(candidateHolder.candidate_transporter).toEqual(null);
    expect(candidateHolder.current_transporter).toEqual({ "candidate_address": roadTransport.publicKeyHash, "transporter_metadata": metadataByte });
  });
  it("Road transporter should confirm the delivery", async () => {
    await Tezos.setSignerProvider(roadTransportSigner);
    const tx = await palmCropNFT.methods.delivery_done(1).send();
    await tx.confirmation(1);
    const storage = await palmCropNFT.storage();
    const candidateHolder = await storage.temporary_transporter.get([1]);
    expect(candidateHolder.candidate_transporter).toEqual(null);
    expect(candidateHolder.current_transporter).toEqual(null);
    expect(candidateHolder.previous_transporter).toEqual([{ "candidate_address": roadTransport.publicKeyHash, "transporter_metadata": metadataByte }]);
  });
});
