# Tezos Grant 00

This repository contains all the smart contracts created for the Green Earth Trading project. It contains 4 differents smart-contracts:

    1. DVP.arl -> Describe an exchange contract. If both parties grant the authorization to the smart-contract to move their ressources (FA2 fungible or non-fungible) the exchange will happen atomicaly.

    2. tkEUR.arl -> A basic FA2 fungible contract. It represent a stable coin, in our case an Euros stable coin.

    3. permits.arl -> FA2 basic permits contract.

    4. product.arl -> A customized FA2 non-fungible contract. It represents either the result of a natural process (i.e. crops) or made (by hand, industrially, etc.). We included composition features that allow us to keep track of what product was used for the making of another product, thus creating a chain until the original product is used. 

## System requirements

For running the test I used the version 16.13.1 of node, 8.1.2 of NPM and v0.12.0 of Taqueria.

## Dev dependicies
The project use the following dependencies: 

    
    "jest": "^29.0.1",

    "@taqueria/plugin-archetype": "^0.12.0",

    "@taqueria/plugin-flextesa": "^0.12.0",

    "@taqueria/plugin-jest": "^0.12.0",

    "@taquito/taquito": "^13.0.s1"


## Testing

You can compile the contracts by running ./compileConract.sh this require to have completium-cli installed.

You also need to have taqueria running for the tests: https://taqueria.io/docs/getting-started/installation/ 

> taq start sandbox local
> npm i
> npm run test

## Demo-script deployment

First execute ``cp conf.example.js conf.js`` to create a conf.js file.

If you want to upload file to IPFS you can update the file with your IPFS infura key.

By default the demo-script will be deployed in the taq sandbox environement. You can create a config in .taq>config.json for the other network deployment.

You can then deploy the smart contracts and generate a "trail" by running: ``node basic_demoScript.js`` or ``node complex_demoScript.js``

The smart contracts and operations done during the demo-script will be stored in the contractDB.json, this can be used for a frontend or backend app using the deployed trail.

## Example - Deployed contracts

Here are the contracts from the ``basic_demoScript.js`` script on the ghostnet network.

For example the paymentToken: https://ghostnet.tzkt.io/KT1WpXF3wr3CgQYK6UWH1gL1pzhjDEqfJdL8/operations/

  Product NFT:

    "palmCrop": "KT1ACnm7a7q5t6fYC7LqW5y68y2w462ZxWFD",

    "cardboardNFT": "KT1ACnm7a7q5t6fYC7LqW5y68y2w462ZxWFD",

    "soapNFT": "KT1Bhx45Pd6U2d37FSUTEBaituRVMECz29YM",

  DVP:

    "DVP-1": "KT1XuK31DzeWFdVc7qifkipjkjXcEVYCjZbr",

    "DVP-2": "KT1VnEbuuhQcSdypY7wF8rjA6S4TAStKHcfz",

    "DVP-3": "KT1APoMMdfLnNeb6eTSDUv1nDNH4o7JMEmr6"

  ``Payment Token: "KT1WpXF3wr3CgQYK6UWH1gL1pzhjDEqfJdL8"``

  Wallet

    "farmer": "tz1MAcF3ibd5Ev8ixZMTHBQZDHeVwXkosXQ4",

    "roadTransport": "tz1bfB52ZXrw4hxgiqdL86k3hJPzFgsfxtoo",

    "factory": "tz1ZP2W9nNL5Wwhryy2tLZKMXvRNyJ22P3TH",

    "store": "tz1V1ER5QRJcQ7Uh1T5SH38A1yjwVnasb4BA"
