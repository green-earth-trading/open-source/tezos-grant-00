import conf from './conf.js';
import * as ipfsClient from "ipfs-http-client";
import { char2Bytes } from "@taquito/utils";


function _checkIfContractDeployed(originTx, originContract) {
    if (!originTx.hash || !originContract) {
        throw new Error('Contract not deployed !');
    }
}

const uploadData = async (data) => {
    const projectId = conf.IPFS_PROJECT_ID;
    const projectSecret = conf.IPFS_PROJECT_SECRET;
    const auth =
        "Basic " +
        Buffer.from(projectId + ":" + projectSecret).toString("base64");

    const client = ipfsClient.create({
        host: "ipfs.infura.io",
        port: 5001,
        protocol: "https",
        headers: {
            authorization: auth,
        },
    });
    const { path } = await client.add(JSON.stringify(data));
    return `ipfs://${path}`;
}

export async function deployAndMintNFT(Tezos, adminSigner, nftUtil, contractOwnerAddress, permits, contractsigner, contractName, type, name, owner, params) {
    // Originate
    await Tezos.setSignerProvider(contractsigner);
    const palmCropOp = await Tezos.contract.originate({
        code: nftUtil.code,
        init: nftUtil.getStorage(
            contractOwnerAddress,
            permits.address,
        ),
    });
    const contract = await palmCropOp.contract();
    _checkIfContractDeployed(palmCropOp, contract);
    const url = await uploadData({
        "name": contractName,
    },);
    const metadataByte = char2Bytes(url);
    let tx = await contract.methods.set_metadata("0", metadataByte).send();
    await tx.confirmation(1);
    Tezos.setSignerProvider(adminSigner);
    tx = await permits.methods.add(contract.address).send();
    await tx.confirmation(1);
    await Tezos.setSignerProvider(contractsigner);
    // Mint
    const urlBis = await uploadData({
        type,
        name,
        owner,
        ...params,
    },);
    const metadataByteMint = char2Bytes(urlBis);
    tx = await contract.methods.mint(contractOwnerAddress, { "": metadataByteMint }, true).send();
    await tx.confirmation(1);
    //
    return { contract, mintHash: tx.hash };
}

export async function declareAndConfirmTransporterForNFT(Tezos, contract, nftId, nftOwnerSigner, transporterAddress, transporterSigner, name, owner) {
    await Tezos.setSignerProvider(nftOwnerSigner);
    let tx = await contract.methods.declare_transporter(nftId, transporterAddress).send();
    await tx.confirmation(1);
    await Tezos.setSignerProvider(transporterSigner);
    const url = await uploadData({
        "type": "truck",
        "setTransporter": tx.hash,
        "name": name,
        "owner": owner,
        "truckType": "Actros Mercedes",
        "truckId": "0 5 847",
        "reception": (Date.now() / 1000),
        "delivery": (Date.now() / 1000) + 2000,
        "from": "5°24’11.3 »N 1°27’29.6 »W",
        "to": "5°28’11.3 »N 1°12’29.6 »W",
        "label": ["Electric vehicle"],
    });
    const metadataByte = char2Bytes(url);
    tx = await contract.methods.confirm_transporter(nftId, metadataByte).send();
    await tx.confirmation(1);
    return tx.hash;
}

/**
 * Leg {
 * walletDebtor,
 * walletCreditor
 * contract,
 * id,
 * amount,
 * signer,
 * }
 */
export async function createDVPAndProceedToExchange(Tezos, DVPSigner, DVPCode, legA, legB) {
    await Tezos.setSignerProvider(DVPSigner);
    const DVPOp = await Tezos.contract.originate({
        code: DVPCode.code,
        init: DVPCode.getStorage(
            legA.contract.address,
            legA.id,
            legA.amount,
            legA.walletDebtor.publicKeyHash,
            legA.walletCreditor.publicKeyHash,
            legB.contract.address,
            legB.id,
            legB.amount,
            legB.walletDebtor.publicKeyHash,
            legB.walletCreditor.publicKeyHash
        ),
    });
    const DVPContract = await DVPOp.contract();
    _checkIfContractDeployed(DVPOp, DVPContract);

    await Tezos.setSignerProvider(legA.signer);
    let tx = await legA.contract.methods
        .update_operators([
            {
                add_operator: {
                    owner: legA.walletDebtor.publicKeyHash,
                    operator: DVPContract.address,
                    token_id: legA.id,
                },
            },
        ])
        .send();
    await tx.confirmation(1);
    await Tezos.setSignerProvider(legB.signer);
    tx = await legB.contract.methods
        .update_operators([
            {
                add_operator: {
                    owner: legB.walletDebtor.publicKeyHash,
                    operator: DVPContract.address,
                    token_id: legB.id,
                },
            },
        ])
        .send();
    await tx.confirmation(1);
    tx = await DVPContract.methods.default().send();
    await tx.confirmation(1);

    return { DVPAddress: DVPContract.address, creationHash: DVPOp.hash, confirmedHash: tx.hash, from: legA.walletDebtor, to: legA.walletCreditor };
}

export async function transporterDeliveryDone(Tezos, transporterSigner, contractNFT, nftID) {
    await Tezos.setSignerProvider(transporterSigner);
    let tx = await contractNFT.methods.delivery_done(nftID).send();
    await tx.confirmation(1);
    return tx.hash;
}